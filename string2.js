module.exports = function(string1) {
    if(string1 === undefined) {
        return [];
    }

    let ipv4List = string1.split('.').map(number => parseInt(number));
    let addressCount = 0;

    for (let index of ipv4List) {
        if (JSON.stringify(index).length === 3) {
            addressCount += 1;
       };
    };
    
    if (addressCount === 4) {
        return (ipv4List);
    } else {
        return ([]);
    }
};