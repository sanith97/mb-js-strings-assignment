module.exports = function(string3) {
    if(string3 === undefined) {
        return "";
    }

    let dateList = string3.split('/');
    let monthinDigit = parseInt(dateList[1]);
    const months = ["January","February","March","April","May","June","July","August","September","October","November","December"];

    if(monthinDigit[0] === 0) {
        return (months[monthinDigit[1] - 1]);
    } else {
        return (months[monthinDigit - 1]);
    }
  
};


