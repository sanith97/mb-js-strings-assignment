module.exports = function(string1) {

    if (string1 === undefined) {
        return "";
    }

    let dividedList = string1.split(".");
    let number = "";
    let numberList = [];

    
    if(dividedList[0][0] === '-') {
        number += "-";
    }

    for (let index of dividedList) {
        for (let char of index) {
            if(parseInt(char) * 0 === 0) {
                number += char;
        }   
    }
    numberList.push(number);
    number = '';
    };

    if (numberList.length === 0) {
        return 0;
    } else {
        return (numberList.join("."));
    }
}


