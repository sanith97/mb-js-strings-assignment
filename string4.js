module.exports = function(string4) {
    if(string4 === undefined) {
        return "";
    }

    let fullName = '';

    for (let name of Object.values(string4)) {
        let lowercaseName = name.toLowerCase();
        let uppercaseLetter = lowercaseName[0].toUpperCase();
        let titleName = uppercaseLetter + lowercaseName.slice(1);
        fullName += titleName + ' ';
    };
    
    return (fullName);  
}
